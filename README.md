# Lehigh University CSE262 - Programming Languages - Homework 5

Solve the following ten questions regarding the Lambda Calculus.

## Question 1

Make all parentheses explicit in these λ- expressions:

1. (λp.pz) λq.w λw.wqzp

    ((λp.(pz)) λq.(w (λw.(((wq)z)p))))

2. λp.pq λp.qp

    (λp.((pq) (λp.(qp))))

## Question 2

In the following expressions say which, if any, variables are bound (and to which λ), and which are free.

1. λs.s z λq.s q
    (λs.((s z) (λq.(s q))))
    - variable s in first expression is bound to first lambda 
    - variable z in first expression is free
    - variable s in second expression is free
    - variable q in second expression is bound to second lambda

2. (λs. s z) λq. w λw. w q z s
    ((λs. (s z)) λq. (w (λw. (((w q) z) s))))
    - variable s in first expression is bound to first lambda
    - variable z is in first expression is free
    - variable w in second expression is free 
    - variable w in third expression is bound to third lambda
    - variables q,z,s after third expression are free

3. (λs.s) (λq.qs)
    - variable s in the first expression is bound to the first lambda
    - variable q in the second expression is bound to the second lambda
    - variable s in the second expression is free

4. λz. (((λs.sq) (λq.qz)) λz. (z z))
    - variable s in second expression is bound to second lambda 
    - variable q in second expression is free
    - variable q in third expression is bound to third lambda
    - variable z in third expression is free 
    - both variables z in fourth expression are bound to fourth lambda


## Question 3

Put the following expressions into beta normal form (use β-reduction as far as possible, α-conversion as needed) assuming left-association.

1. (λz.z) (λq.q q) (λs.s a)

    (((λz.z) (λq.q q)) (λs.s a))
    (z) [z -> (λq.qq)]
    ((λq.qq) (λs.sa))
    (qq) [q -> (λs.sa)]
    ((λs.sa) (λs.sa))
    (sa) [s -> (λt.tb)]
    ((λt.tb)a)
    (tb) [t -> a]
    (a b)

2. (λz.z) (λz.z z) (λz.z q)

    (((λz.z) (λz.z z)) (λz.z q))
    (z) [z -> (λy.yy)]
    ((λy.yy) (λz.zq))
    (yy) [y -> (λx.xq)]
    ((λx.xq)(λx.xq))
    (xq) [x -> (λw.wp)]
    ((λw.wp)q)
    (wp) [q -> w]
    (qp)

3. (λs.λq.s q q) (λa.a) b
    (((λs.λq.s q q) (λa.a)) b)
    (λq.sqq) [s -> (λa.a)]
    ((λq.(λa.a)qq)b)
    ((λa.a)qq) [q -> b]
    (((λa.a)b)b)
    a [a -> c]
    (cb)

4. (λs.λq.s q q) (λq.q) q
    (((λs.λq.s q q) (λq.q)) q)
    (λq.sqq) [s -> (λp.p)]
    ((λq.(λp.p)q q)q)
    ((λp.p)qq) [q -> o]
    (((λp.p)o)o)
    p [p -> n]
    (no)

5. ((λs.s s) (λq.q)) (λq.q)
    (((λs.s s) (λq.q)) (λq.q))
    (ss) [s -> (λq.q)]
    ((λq.q)(λq.q))
    q [q -> (λp.p)]
    (λp.p)(λq.q)
    p [p -> (λo.o)]
    (λo.o)



## Question 4

1. Write the truth table for the or operator below.
    T T -> T
    T F -> T
    F T -> T
    F F -> F


2. The Church encoding for OR = (λp.λq.p p q)

Prove that this is a valid "or" function by showing that its output matches the truth table above. You will have 4 derivations. For the first derivation, show the long-hand solution (don't use T and F, use their definitions). For the other 3 you may use the symbols in place of the definitions. 
    TT
    (λp.λq.p p q) T T
    (((λp.λq.p p q) (λaλb.a)) (λaλb.a))
    (λp,ppq) [p -> (λaλb.a)]
    ((λq.(λaλb.a)(λaλb.a)q) (λaλb.a))
    (λaλb.a)(λaλb.a) [q -> (λcλd.c)]
    (((λaλb.a)(λaλb.a))(λcλd.c))
    (λb.a) [a -> (λeλf.e)]
    (λb.(λeλf.e)) (λcλd.c)
    (λeλf.e) [b -> (λcλd.c)]
    (λeλf.e)
        T
    
    TF
    (λp.λq.p p q) T F
    (((λp.λq.p p q) T) F)
    (λq.ppq) [p -> T]
    (λq.TTq) F
    (TTq) [q -> F]
    (TTF) 
        T -> (T = (λaλb.a))
    FT
    (λp.λq.p p q) F T
    (((λp.λq.p p q) F) T)
    (λq.ppq) [p -> F]
    (λq.FFq) F
    (FFq) [q -> T]
    (FFT) -> (F = (λaλb.b))
    T

    FF
    (λp.λq.p p q) F F
    (((λp.λq.p p q) F) F)
    (λq.ppq) [p -> F]
    (λq.FFq) F
    (FFq) [q -> F]
    (FFF) -> (F = (λaλb.b))
    F




## Question 5

Derive a lambda expression for the NOT operator. Explain how this is similar to an IF statement.
    F -> T
    T -> F
    (λp.p F T) as T returns first input and F returns second input
    This lambda expression is similar to an if statement because if the input is true, the expression will
    return the first operator after, which is a false. However, if the input is false, the expression will 
    return the second (or next) operator as with if statements. In other words, if the condition given is true then we do this else if condition is false we do something else. 





## Instructions

1. Fork the relevant repository into your own namespace. [Instructions](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)
2. Set your forked repository visibility to private. [Instructions](https://docs.gitlab.com/ee/public_access/public_access.html#how-to-change-project-visibility)
3. Add user "LehighCSE262" as a member to the project with "maintainer" access-level. [Instructions](https://docs.gitlab.com/ee/user/project/members/#add-a-user). 
4. Clone your newly forked repository. [Instructions](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) 
5. Answer the questions here in the readme or in another document. Upload your solutions here to Gitlab.